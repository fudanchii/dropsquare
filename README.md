# DROPSQUARE

Here's DropSquare, an app that track and backup your files.

It's still on its early stage. And not working as of currently.


## Stack used

- Ruby on Rails
- SQLite (for now)
- Redis (to be used as queue storage for Sidekiq, and message_bus)


## How to setup


```
$ bundle install
$ rails db:migrate
$ foreman start
```


Nurahmadie <nurahmadie@gmail.com>

