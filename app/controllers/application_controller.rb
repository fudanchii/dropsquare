class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  protected

  def current_page
    page = params[:page].to_i
    page <= 0 ? 1 : page
  end

  def assign_user
    @user = current_user
    @profiles = current_user.profiles.with_latest_scan.page(current_page)
    @profile_count = current_user.profiles.count
  end
end
