class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :assign_user

  def index
    @newprofile = current_user.profiles.build
  end

  def create
    current_user.profiles.create!(profile_params)
    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { head :created }
    end
  end

  def show
    @profile = @profiles.find(params[:id])
    @files = @profile.latest_scan.blobs
  end

  def scan
    @profile = @profiles.find(params[:id])
    jid = ScanWorker.perform_async(current_user.id, @profile.name)
    render json: { watch: "/scan-#{jid}" }
  end

  private

  def profile_params
    params.require(:profile).permit(:name, :directory_path)
  end
end
