// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//

function showNewProfileForm(npmdl) {
  npmdl.classList.add('is-active');
}

function hideNewProfileForm(npmdl) {
  npmdl.classList.remove('is-active');
}

function doScan(target) {
  target.classList.add('is-loading');
  Rails.ajax({
    type: 'get',
    url: '/' + window.current_profile_id + '/scan',
    success(rsp, status, xhr) {
      if (rsp.watch) { doWatch(rsp.watch, target); }
    },
    error(rsp, status, xhr) {
      target.classList.remove('is-loading');
    }
  });
}

function doWatch(channel, btn) {
  MessageBus.subscribe(channel, (data) => {
    btn.classList.remove('is-loading');
    MessageBus.unsubscribe(channel);
  }, 0);
}

function is(obj) {
  return {
    aFunction() {
      return obj.constructor === Function;
    },
    aDOM() {
      return obj.addEventListener && is(obj.addEventListener).aFunction();
    }
  };
}

function on(dom, event, fn) {
  if (dom && is(dom).aDOM()) {
    dom.addEventListener('click', fn);
  }
  return dom;
}

document.addEventListener('DOMContentLoaded', (ev) => {
  let npbtn = document.getElementById('add-newprofile');
  let npmdl = Rails.$('#newprofile-modal.modal')[0];
  let mdhnds = Rails.$('.modal-background,.modal-close');

  let scanbtn = document.getElementById('btn-scan');
  let fnewbtn = document.getElementById('btn-filter-new');
  let frmdbtn = document.getElementById('btn-filter-removed');
  let fchdbtn = document.getElementById('btn-filter-changed');
  let fuchbtn = document.getElementById('btn-filter-unchanged');

  on(npbtn, 'click', (ev) => showNewProfileForm(npmdl));
  on(scanbtn, 'click', (ev) => doScan(scanbtn));
  on(fnewbtn, 'mousedown', (ev) => fnewbtn.classList.toggle('is-active'));
  on(frmdbtn, 'mousedown', (ev) => frmdbtn.classList.toggle('is-active'));
  on(fchdbtn, 'mousedown', (ev) => fchdbtn.classList.toggle('is-active'));
  on(fuchbtn, 'mousedown', (ev) => fuchbtn.classList.toggle('is-active'));

  mdhnds.forEach((c,i,a) => {
    c.addEventListener('click', (ev) => hideNewProfileForm(npmdl));
  });

  Rails.refreshCSRFTokens();
  MessageBus.start();
});
