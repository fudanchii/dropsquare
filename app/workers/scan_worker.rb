class ScanWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'scanqueue', retry: false

  def perform(user_id, profile_name)
    prof = Profile.find_by! user_id: user_id, name: profile_name
    prof.scan
  ensure
    MessageBus.publish("/scan-#{jid}", true)
  end
end
