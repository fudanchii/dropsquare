class Scan < ApplicationRecord
  belongs_to :profile
  has_many :blobs

  before_create :put_hash

  private

  def put_hash
    self.scan_hash = SecureRandom.random_bytes 16
  end
end

# == Schema Information
#
# Table name: scans
#
#  id             :integer          not null, primary key
#  start_time     :datetime
#  end_time       :datetime
#  scan_hash      :binary           not null
#  profile_id     :integer
#  directory_tree :binary
#  byte_size      :integer
#  files_count    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_scans_on_profile_id  (profile_id)
#  index_scans_on_scan_hash   (scan_hash) UNIQUE
#
