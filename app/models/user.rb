class User < ApplicationRecord
  include Gravtastic
  gravtastic secure: true, size: 164, filetype: :jpg

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :profiles, dependent: :destroy

  def total_files_count
    Rails.cache.fetch("#{id}/files_count") do
      profiles.joins(:latest_scan)
        .select('user_id, sum(scans.files_count) files_count')
        .group('user_id')
        .first&.files_count
    end.to_i
  end

  def total_storage_used
    Rails.cache.fetch("#{id}/storage") do
      profiles.joins(:latest_scan)
        .select('user_id, sum(byte_size) storage')
        .group('user_id')
        .first&.storage
    end.to_i
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
