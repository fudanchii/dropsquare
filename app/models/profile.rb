require 'dropsquare/scanner'

class Profile < ApplicationRecord
  has_many :scans

  belongs_to :user
  belongs_to :latest_scan, class_name: 'Scan', optional: true

  validates :name, :directory_path, presence: true

  scope :with_latest_scan, -> {
    left_outer_joins(:latest_scan)
      .select(<<-EOQ)
        scans.start_time as scan_start_time,
        scans.end_time as scan_end_time,
        scans.byte_size as byte_size,
        scans.files_count as files_count,
        profiles.*
      EOQ
  }

  def scan
    scanner = DropSquare::Scanner.new(latest_scan, Settings.scan.basedir)
    result = scanner.scan(directory_path)
    transaction do
      cscan = scans.create!(result[:scan_params])
      result[:blobs].each do |blob|
        blob.scan_id = cscan.id
        blob.save!
      end
      self.latest_scan = cscan
      save!
    end
  end
end

# == Schema Information
#
# Table name: profiles
#
#  id              :integer          not null, primary key
#  name            :string
#  directory_path  :string
#  last_scanned_at :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  latest_scan_id  :integer
#
# Indexes
#
#  index_profiles_on_latest_scan_id    (latest_scan_id)
#  index_profiles_on_user_id           (user_id)
#  index_profiles_on_user_id_and_name  (user_id,name) UNIQUE
#
