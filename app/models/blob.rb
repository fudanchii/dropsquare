class Blob < ApplicationRecord
  belongs_to :scan, optional: true
  has_many   :diffs, class_name: 'Blob', foreign_key: 'base_id'
  belongs_to :base, class_name: 'Blob', optional: true

  enum diff_format: [ :diff, :vcdiff ]
  enum status: [ :fadded , :fremoved, :fchanged, :funchanged ]

  scope :sorted_link, -> { order(linked_index: :asc) }

  def full_content_path
  end
end

# == Schema Information
#
# Table name: blobs
#
#  id           :integer          not null, primary key
#  path         :string
#  content_hash :binary           not null
#  base_id      :integer
#  link_index   :integer
#  scan_id      :integer
#  diff_format  :integer
#  content_path :string
#  byte_size    :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  status       :integer          default("0")
#  modified_at  :datetime
#
# Indexes
#
#  index_blobs_on_base_id                          (base_id)
#  index_blobs_on_content_hash                     (content_hash)
#  index_blobs_on_path_and_base_id_and_link_index  (path,base_id,link_index) UNIQUE
#  index_blobs_on_scan_id                          (scan_id)
#
