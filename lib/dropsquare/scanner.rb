require 'dropsquare/state'

module DropSquare
  class Scanner
    def initialize(state, basedir)
      @basedir = basedir
      @cstate = DropSquare::State.new state
    end

    def scan(workingdir)
      @workingdir = File.join(@basedir, workingdir, '/')
      result = { scan_params: {} }
      result[:scan_params][:start_time] = DateTime.now.utc
      scresult = recscan({ blobs: [], byte_size: 0, files_count: 0 }, @workingdir)
      result[:scan_params][:byte_size] = scresult[:byte_size]
      result[:scan_params][:files_count] = scresult[:files_count]
      result[:scan_params][:end_time] = DateTime.now.utc
      result[:blobs] = @cstate.match_removed(scresult[:blobs])
      result
    end

    private

    # recscan will recursively scan the given directory,
    # but sometimes there are files which are too funky
    # to get scanned (I'm looking at you dead symlink <_<).
    # In such case, just ignore the exception and continue
    # with the next file.
    def recscan(result, pwd)
      Dir.glob(File.join(pwd, '*'), File::FNM_DOTMATCH).each do |entry|
        next if File.basename(entry).eql?('.') || File.basename(entry).eql?('..')
        if File.directory?(entry)
          result = recscan result, entry
          next
        end
        begin
          stats = File.stat(entry)
          result[:byte_size] += stats.size
          result[:files_count] += 1
        rescue
          next
        end
        result[:blobs] << @cstate.match(entry, stats, @workingdir)
      end
      result
    end
  end
end
