module DropSquare
  class State
    def initialize(rec)
      @istate = rec || ::Scan.new
      @blobs = @istate.blobs.each_with_object({}) { |v, m| m[v.path] = v }
      @digest = OpenSSL::Digest::SHA256.new
    end

    def match(entry, entry_stat, prefix)
      blob = ::Blob.new
      nopref = entry.dup
      nopref.slice! prefix
      oent = @blobs[nopref]
      if oent
        blob.base_id = oent.base_id
        chash = calc_digest entry
        blob.diff_format = oent.diff_format
        if chash.eql?(oent.content_hash)
          blob.status = "funchanged"
          blob.content_hash = oent.content_hash
          blob.content_path = oent.content_path
        else
          blob.satus = "fchanged"
          blob.contenthash = chash
          blob.content_path = create_diff(oent.full_content_path, entry, oent.diff_format)
        end
      else
        blob.status = "fadded"
        blob.content_hash = calc_digest entry
        blob.content_path = store_content entry
        blob.diff_format = diff_format entry
      end
      blob.path = nopref
      blob.byte_size = entry_stat.size
      blob.modified_at = entry_stat.mtime
      blob.link_index = oent&.link_index.to_i + 1
      blob
    end

    def match_removed(entries)
      old_entries = @blobs.keys
      new_entries = entries.map &:path
      removed = old_entries - new_entries
      removed_blobs = @istate.blobs.each_with_object([]) do |b, arr|
        next unless removed.include?(b.path)
        nb = b.dup
        nb.status = "fremoved"
        nb.link_index += 1
        nb.content_path = nil
        arr << nb
      end
      entries + removed_blobs
    end

    private

    def calc_digest(fname)
      @digest.reset
      File.open(fname) do |fin|
        fin.each_line { |line| @digest << line }
      end
      @digest.digest
    end

    def create_diff(old_path, new_path, format)
    end

    def store_content(entry)
    end

    def diff_format(entry)
      "diff"
    end
  end
end
