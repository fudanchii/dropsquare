# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170601165652) do

  create_table "blobs", force: :cascade do |t|
    t.string "path"
    t.binary "content_hash", null: false
    t.integer "base_id"
    t.integer "link_index"
    t.integer "scan_id"
    t.integer "diff_format"
    t.string "content_path"
    t.integer "byte_size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.datetime "modified_at"
    t.index ["base_id"], name: "index_blobs_on_base_id"
    t.index ["content_hash"], name: "index_blobs_on_content_hash"
    t.index ["path", "base_id", "link_index"], name: "index_blobs_on_path_and_base_id_and_link_index", unique: true
    t.index ["scan_id"], name: "index_blobs_on_scan_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "name"
    t.string "directory_path"
    t.datetime "last_scanned_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "latest_scan_id"
    t.index ["latest_scan_id"], name: "index_profiles_on_latest_scan_id"
    t.index ["user_id", "name"], name: "index_profiles_on_user_id_and_name", unique: true
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "scans", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.binary "scan_hash", null: false
    t.integer "profile_id"
    t.binary "directory_tree"
    t.integer "byte_size"
    t.integer "files_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profile_id"], name: "index_scans_on_profile_id"
    t.index ["scan_hash"], name: "index_scans_on_scan_hash", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
