class AlterProfileAddLatestScan < ActiveRecord::Migration[5.1]
  def change
    add_reference :profiles, :latest_scan, foreign_key: { to_table: :scans }
    remove_column :profiles, :byte_size
  end
end
