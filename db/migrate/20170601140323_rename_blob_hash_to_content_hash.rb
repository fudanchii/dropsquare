class RenameBlobHashToContentHash < ActiveRecord::Migration[5.1]
  def change
    remove_index :blobs, :hash
    rename_column :blobs, :hash, :content_hash
    add_index :blobs, :content_hash, unique: true
  end
end
