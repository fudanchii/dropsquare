class AddColumnBlobStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :blobs, :status, :integer, default: 0
  end
end
