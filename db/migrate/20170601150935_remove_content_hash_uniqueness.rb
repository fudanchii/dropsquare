class RemoveContentHashUniqueness < ActiveRecord::Migration[5.1]
  def change
    remove_index :blobs, :content_hash
    add_index :blobs, :content_hash
  end
end
