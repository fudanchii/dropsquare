class RenameHashToScanHash < ActiveRecord::Migration[5.1]
  def change
    rename_column :scans, :hash, :scan_hash
  end
end
