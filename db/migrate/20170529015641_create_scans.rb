class CreateScans < ActiveRecord::Migration[5.1]
  def change
    create_table :scans do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.binary :hash, null: false
      t.index :hash, unique: true
      t.references :profile, foreign_key: true
      t.binary :directory_tree
      t.integer :byte_size
      t.integer :files_count
      t.timestamps
    end
  end
end
