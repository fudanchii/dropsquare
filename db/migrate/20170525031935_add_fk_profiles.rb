class AddFkProfiles < ActiveRecord::Migration[5.1]
  def change
    add_reference :profiles, :user, index: true
    add_foreign_key :profiles, :users
    add_index :profiles, [:user_id, :name], unique: true
  end
end
