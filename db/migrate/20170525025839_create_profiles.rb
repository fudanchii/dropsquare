class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :directory_path
      t.datetime :last_scanned_at
      t.integer :byte_size
      t.timestamps
    end
  end
end
