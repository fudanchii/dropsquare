class AddModificationDate < ActiveRecord::Migration[5.1]
  def change
    add_column :blobs, :modified_at, :datetime
  end
end
