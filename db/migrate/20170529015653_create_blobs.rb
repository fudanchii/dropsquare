class CreateBlobs < ActiveRecord::Migration[5.1]
  def change
    create_table :blobs do |t|
      t.string :path
      t.binary :hash, null: false
      t.references :base, index: true
      t.integer :link_index
      t.index :hash, unique: true
      t.index [:path, :base_id, :link_index], unique: true
      t.references :scan, foreign_key: true
      t.integer :diff_format
      t.string :content_path
      t.integer :byte_size
      t.timestamps
    end
  end
end
