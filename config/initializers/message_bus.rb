require 'message_bus'

rediscfg = Settings.redis.message_bus

if Rails.env.test?
  MessageBus.configure backend: memory
else
  MessageBus.redis_config = {
    driver: :hiredis,
    host: rediscfg.host,
    port: rediscfg.port,
    password: rediscfg.password,
    db: rediscfg.db
  }
end
