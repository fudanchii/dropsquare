stredis = Settings.redis.sidekiq
rediscfg = { url: "redis://#{stredis.host}:#{stredis.port}/#{stredis.db}" }

Sidekiq.configure_server do |config|
  config.redis = rediscfg
end

Sidekiq.configure_client do |config|
  config.redis = rediscfg
end
