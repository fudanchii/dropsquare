require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :users

  mount Sidekiq::Web => '/admin/sidekiq'

  resources :profiles, path: '/' do
    get 'scan', action: :scan, on: :member
  end

  devise_scope :user do
    get 'users/sign_out', to: 'devise/sessions#destroy'
  end


  scope '/v1', format: true, constraints: { format: 'json' } do
    resources :profiles
  end


  root to: 'profiles#index'
end
